const express = require( 'express' );
const http = require( 'http' );
const WebSocket = require( 'ws' );

const port = 6969;
const server = http.createServer( express );
const wss = new WebSocket.Server( { server } );

let list = [],
	index = 0;

wss.on( 'connection', function connection( ws ) {
	ws.on( 'message', function incoming( data ) {
		data = JSON.parse( data );

		let answer;

		switch ( data.action ) {
			case 'addItem':
				list[ index ] = data.text;

				answer = {
					'action': data.action,
					'index': index,
					'text': data.text
				};

				index++;

				wss.clients.forEach( function each( client ) {
					if ( client.readyState === WebSocket.OPEN ) {
						client.send( JSON.stringify( answer ) );
					}
				} )

				break;
			case 'loadList':
				answer = {
					'action': data.action,
					'list': list
				};

				ws.send( JSON.stringify( answer ) );

				break;
			case 'deleteItem':
				delete list[ data.index ];

				answer = {
					'action': data.action,
					'index': data.index
				};

				wss.clients.forEach( function each( client ) {
					if ( client.readyState === WebSocket.OPEN ) {
						client.send( JSON.stringify( answer ) );
					}
				} )

				break;
			case 'editItem':
				list[ data.index ] = data.text;

				answer = {
					'action': data.action,
					'index': data.index,
					'text': data.text
				};

				wss.clients.forEach( function each( client ) {
					if ( client.readyState === WebSocket.OPEN ) {
						client.send( JSON.stringify( answer ) );
					}
				} )

				break;
		}
	} )
} );

server.listen( port, function() {
	console.log( `Server is listening on ${port}!` )
} );